<?php

namespace App\Controller;

use App\Repository\UserRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /** @var SerializerInterface  */
    protected $serializer;

    /**
     * NotificationController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/users")
     */
    public function index(UserRepository $userRepository): Response
    {
        $json = $this->serializer->serialize($userRepository->findAll(), 'json');

        return new JsonResponse($json, 200, [], true);
    }
}
