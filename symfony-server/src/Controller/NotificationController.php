<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use JMS\Serializer\SerializerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class NotificationController extends AbstractController
{
    /** @var SerializerInterface  */
    protected $serializer;

    /**
     * NotificationController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/get-notifications")
     */
    public function index(): Response
    {
        $json = $this->serializer->serialize($this->getUser()->getNotifications(), 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/api/notifications", methods={"POST"})
     */
    public function create(Request $request, EntityManagerInterface $manager, UserRepository $userRepository): Response
    {
        $body = json_decode($request->getContent(), true);
        $user = $userRepository->find($body['userId']);

        $notification = new Notification();
        $notification->setDescription($body['description'])->setStatus(0)->setIcon('far fa-comments icon')->setUser($user);

        $manager->persist($notification);
        $manager->flush();

        $json = $this->serializer->serialize($notification, 'json');

        // TODO Put this into a service
        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();
        $channel->queue_declare('notifications', false, false, false, true);

        $msg = new AMQPMessage($json);
        $channel->basic_publish($msg, '', 'notifications');

        return new JsonResponse($json, 201, [], true);
    }
}
