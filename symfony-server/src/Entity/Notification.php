<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entité notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=50, nullable=true)
     */
    private $color;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="notifications")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="emplacement", type="string", length=50, nullable=true)
     */
    private $emplacement;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=1000)
     */
    private $icon;

    /**
     * @var boolean
     * @ORM\Column(name="pushed", type="boolean", nullable=true)
     */
    private $pushed;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = 1;
        $this->pushed = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Notification
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Notification
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Notification
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Notification
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }


    /**
     * Set link
     *
     * @param string $link
     * @return Notification
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Notification
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        if(!$this->icon)
            $this->icon = "ion-android-alert icon";
        return $this->icon;
    }

    /**
     * @return boolean
     */
    public function isPushed()
    {
        return $this->pushed;
    }

    /**
     * @param $pushed
     * @return $this
     */
    public function setPushed($pushed)
    {
        $this->pushed = $pushed;

        return $this;
    }

    /**
     * Get pushed
     *
     * @return boolean
     */
    public function getPushed()
    {
        return $this->pushed;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     *
     * @return Notification
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Notification
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    // far fa-pause adb5bd

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        if(!$this->color){
            switch ($this->getIcon()){
                case "ion-android-alert icon":
                    $this->color = '#e74c3c';
                case "ion-home icon":
                    $this->color = '#007bbb';
                case "ion-person-add iconn":
                    $this->color = '#ffba62';
                case "ion-ios-checkmark icon":
                    $this->color = '#2ecc71';
                case "ion-document-text icon":
                    $this->color = '#33ccff';
                case "ion-ios-close icon":
                    $this->color = '#9b59b6';
                case "far fa-smile-wink icon":
                    $this->color = '#9b59b6';
                case "far fa-pause icon":
                    $this->color = '#adb5bd';
            }
        }

        return $this->color;
    }

}
