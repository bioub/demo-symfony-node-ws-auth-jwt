<?php

namespace App\DataFixtures;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $notification1 = new Notification();
        $notification1->setDescription('Texte Notification 1')->setStatus(0)->setIcon('far fa-comments icon');
        $manager->persist($notification1);

        $notification2 = new Notification();
        $notification2->setDescription('Texte Notification 2')->setStatus(0)->setIcon('far fa-comments icon');
        $manager->persist($notification2);

        $user1 = new User();
        $user1->setEmail('romain.bohdanowicz@wearedevs.fr')
            ->setPassword($this->passwordHasher->hashPassword(
                $user1,
                '123456'
            ))
            ->addNotification($notification1)
            ->addNotification($notification2);
        $manager->persist($user1);

        $notification3 = new Notification();
        $notification3->setDescription('Texte Notification 3')->setStatus(0)->setIcon('far fa-comments icon');
        $manager->persist($notification3);

        $user2 = new User();
        $user2->setEmail('romain.bohdanowicz@formation.tech')
            ->setPassword($this->passwordHasher->hashPassword(
                $user2,
                '123456'
            ))
            ->addNotification($notification3);
        $manager->persist($user2);

        $manager->flush();
    }
}
