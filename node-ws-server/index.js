/** @type {import('socket.io').Server} */
const io = require('socket.io')(4000, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"]
  },
});
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const amqp = require('amqplib/callback_api');

const publicPath = path.resolve(__dirname, '../symfony-server/config/jwt/public.pem');
const publicKey = fs.readFileSync(publicPath);

io.use((socket, next) => {
  const token = socket.handshake.auth.token;

  jwt.verify(token, publicKey, (err, decoded) => {
    if (err) {
      const err = new Error('Unauthorized');
      err.data = { message: 'Token is not valid' };
      next(err);
    } else {
      socket.data.username = decoded.username;
      next();
    }
  });
});

// io.on("connection", (socket) => {
// sockets.push(socket);

// either with send()
//socket.send("Hello!");

// or with emit() and custom event names
//socket.emit("greetings", "Hey!", { "ms": "jane" }, Buffer.from([4, 3, 3, 1]));

// handle the event sent with socket.send()
//socket.on("message", (data) => {
//  console.log(data);
//});

// handle the event sent with socket.emit()
//socket.on("salutations", (elem1, elem2, elem3) => {
//  console.log(elem1, elem2, elem3);
//});
// });

amqp.connect('amqp://localhost', (err, connection) => {
  if (err) {
    return console.log(err);
  }

  connection.createChannel((err, channel) => {
    if (err) {
      return console.log(err);
    }

    const queue = 'notifications';

    channel.assertQueue(queue, {
      durable: false,
      autoDelete: true,
    });

    channel.consume(queue, (msg) => {
      const notification = JSON.parse(msg.content.toString());
      
      for (const socket of io.sockets.sockets.values()) {
        if (socket.data.username === notification.user.email) {
          socket.emit('notification', notification);
        }
      }
    })
  });
});
