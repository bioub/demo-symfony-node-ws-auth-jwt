import EmitNotification from "./EmitNotification";
import Notifications from "./Notifications";

function Dashboard() {
  return (
    <div className="Dashboard">
      <h2>Dashboard</h2>
      <Notifications />
      <EmitNotification />
    </div>
  );
}

export default Dashboard;
