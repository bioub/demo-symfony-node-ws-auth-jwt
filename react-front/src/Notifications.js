import { useEffect, useRef, useState } from 'react';
import {io} from 'socket.io-client';

function Notifications() {
  const [notifications, setNotifications] = useState([]);
  const socketRef = useRef();

  useEffect(() => {
    (async () => {
      const res = await fetch('http://localhost:8000/api/get-notifications', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      });
      
      if (res.status === 200) {
        const body = await res.json();
        setNotifications(body);
      } else {
        localStorage.removeItem('token');
        window.location.reload();
      }
    })();
  }, []);

  useEffect(() => {
    if (!socketRef.current) {
      socketRef.current = io("ws://localhost:4000", {
        auth: {
          token: localStorage.getItem('token')
        }
      });
    }
    socketRef.current.on('notification', (newNotification) => {
      setNotifications([
        ...notifications,
        newNotification,
      ]);
    });
  }, [notifications]);

  return (
    <div className="Notifications">
      <h3>Notifications</h3>
      <ul>
        {notifications.map((n) => <li key={n.id}>{n.description}</li>)}
      </ul>
    </div>
  );
}

export default Notifications;
