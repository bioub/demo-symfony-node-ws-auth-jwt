import { useEffect, useState } from "react";

function EmitNotification() {
  const [users, setUsers] = useState([]);
  const [notification, setNotification] = useState({
    userId: null,
    description: '',
    icon: 'far fa-description',
    status: 0,
  });

  useEffect(() => {
    (async () => {
      const res = await fetch('http://localhost:8000/api/users', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      });

      const body = await res.json();
      setUsers(body);
    })();
  }, []);

 
function handleChange(event) {
  console.log(event)
    setNotification({
      ...notification,
      [event.target.name]: event.target.value,
    });
  }

  async function handleSubmit(event) {
    event.preventDefault();

    const res = await fetch('http://localhost:8000/api/notifications', {
      method: "POST",
      body: JSON.stringify(notification),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      }
    });

    const body = await res.json();
    console.log(body);
  }

  return (
    <div className="EmitNotification">
      <h3>Emit Notification</h3>
      <form onSubmit={handleSubmit}>
        <select name="userId" onChange={handleChange}>
          <option></option>
          {users.map((u) => <option value={u.id} key={u.id}>{u.email}</option>)}
        </select>
        <textarea name="description" onChange={handleChange}></textarea>
        <button>Send</button>
      </form>
    </div>
  );
}

export default EmitNotification;
