import { useState } from "react";

function Login() {
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });

  const [error, setError] = useState('');

  function handleChange(event) {
    setCredentials({
      ...credentials,
      [event.target.name]: event.target.value,
    });
  }

  async function handleSubmit(event) {
    event.preventDefault();
    const res = await fetch('http://localhost:8000/api/login_check', {
      method: "POST",
      body: JSON.stringify(credentials),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    const body = await res.json();

    if (res.status !== 200) {
      setError(body.message);
    } else {
      localStorage.setItem('token', body.token);
      window.location.reload(); // TODO retirer le hard refresh (context, redux)
    }
  }

  return (
    <div className="Login">
      <h2>Login Page</h2>
      <form onSubmit={handleSubmit}>
        {error && <div>{error}</div>}
        <div>
          <input name="username" placeholder="Email" onChange={handleChange} />
        </div>
        <div>
          <input name="password" type="password" placeholder="Password" onChange={handleChange} />
        </div>
        <div>
          <button>Login</button>
        </div>
      </form>
    </div>
  );
}

export default Login;
