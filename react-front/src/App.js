import { useEffect, useState } from 'react';

import Dashboard from './Dashboard';
import Login from './Login';

function App() {
  const [isLogged, setLogged] = useState(false);

  useEffect(() => {
    setLogged(!!localStorage.getItem('token'));
  }, []);

  return <div className="App">{isLogged ? <Dashboard /> : <Login />}</div>;
}

export default App;
